# glmer_mixed_effects_logistic

Example code to perform mixed-effects repeated measures logistic regression using glmer()

* Adapted from

        https://stats.idre.ucla.edu/r/dae/mixed-effects-logistic-regression/
    
* In a Bayesian framework can use rstanarm::stan_glmer()    